
doc:


    git clone https://gitlab.com/joussin/python-test-injection.git

    goto branch:
        basic_injection for basic injection example:
        git checkout basic_injection


        other_injection_method for other injection example:
        git checkout other_injection_method

    see:
        https://www.it-swarm.dev/fr/python/quest-ce-quune-methode-pythonic-pour-linjection-de-dependance/1054351747/

install:

    make install

    or

    pip install -U Flask

start:

    make start

    or

    FLASK_APP=main.py FLASK_ENV=development flask run
