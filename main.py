from flask import Flask, render_template
app = Flask(__name__)

@app.route("/")
def index():
    contents=open("readme.md","r").read()
    return render_template('index.html', doc=contents)